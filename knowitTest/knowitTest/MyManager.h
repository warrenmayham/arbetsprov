//
//  MyManager.h
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyManager : NSObject

+ (id)sharedManager;

@property (strong, nonatomic) NSString *currPhoneNumber;

- (void) setPhoneNumber: (NSString *)number;

@end
