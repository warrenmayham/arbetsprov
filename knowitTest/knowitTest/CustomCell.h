//
//  CustomCell.h
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-09-13.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *errorMessage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
