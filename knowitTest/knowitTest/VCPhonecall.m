//
//  ThirdViewController.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "VCPhonecall.h"
#import "MyManager.h"

@interface VCPhonecall()

@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation VCPhonecall

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
}

- (void)viewWillAppear:(BOOL)animated{
    MyManager *sharedManager = [MyManager sharedManager];
    
    if(sharedManager.currPhoneNumber != nil){
        self.textField.text = sharedManager.currPhoneNumber;
    }else{
        self.textField.text = nil;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.textField resignFirstResponder];
    MyManager *sharedManager = [MyManager sharedManager];
    
    if(self.textField.text != nil){
        [sharedManager setPhoneNumber:self.textField.text];
    }else{
        [sharedManager setPhoneNumber:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dialNumber:(id)sender{  //Metoden sköter ett bekräftelsemeddelande samt själva samtalet
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Confirmation"
                                  message:@"Are you sure you want to call this number?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Yes"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                                    //Här ringer den numret om det är ett giltigt nummer (ios har en egen koll för det, så min är lite överflödig)
                             if(self.textField.text.length != 0){
                                 NSString *phoneNumber = [NSString stringWithFormat:@"tel:%@", self.textField.text];
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                             }
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
