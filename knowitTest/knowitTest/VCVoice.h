//
//  VCVoice.h
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-09-07.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VCVoice : UIViewController <AVSpeechSynthesizerDelegate>

- (IBAction)playPauseButton:(id)sender;
- (IBAction)stopButton:(id)sender;

@end
