//
//  MyManager.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "MyManager.h"

@implementation MyManager

/*
En singleton som hanterar kommunikation mellan klasser
*/

- (void) setPhoneNumber: (NSString *)number{
    
    self.currPhoneNumber = number;
}

#pragma mark Singleton Methods

+ (id)sharedManager {
    static MyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}


@end
