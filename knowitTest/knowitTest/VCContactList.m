//
//  VCContactList.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "VCContactList.h"
#import "MyManager.h"
@import Contacts;

/*
 VCContactlist hämtar mobilens kontakter via Contact Framework och lägger upp dem i en tableview
 kommunikationen mellan VCContactlist och VCPhonebook sköts via appdelegate
 
 Har en uppenbar brist här i att man måste välja en kontakt för att komma tillbaka. 
 Borde ha lagt in en cancel-knapp, men beslutade att det inte var värt att dra ut på tiden ännu mer.
 */

@implementation VCContactList

- (void) viewDidLoad{
    
    self.groupOfContacts = [@[] mutableCopy];
    [self getAllContacts];
}

- (void) getAllContacts{
    if([CNContactStore class]){
        
        CNContactStore *contactsList = [[CNContactStore alloc] init];
        
        NSArray *keysToFetch = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey];
        CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
        [contactsList enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            [self.groupOfContacts addObject:contact];
        }];
        
        /* how to get contact info
         CNContact *firstContact = self.groupOfContacts[0];
         NSString *contactName = [NSString stringWithFormat:@"%@ %@", firstContact.givenName, firstContact.familyName];
         */
        
    }
}

#pragma mark - TableView Methods

//Här hämtas numret för kontakten som valts och skickas vidare till VCPhonebook via MyManager
- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CNContact *contact = self.groupOfContacts[indexPath.row];
    
    NSArray *contactNumberArr = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    
    MyManager *sharedManager = [MyManager sharedManager];

    if (contactNumberArr != nil && contactNumberArr.count != 0){
        [sharedManager setPhoneNumber:contactNumberArr[0]];
    }else{
        [sharedManager setPhoneNumber:nil];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int num = (int)self.groupOfContacts.count;
    
    return num;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    CNContact *firstContact = self.groupOfContacts[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", firstContact.familyName, firstContact.givenName];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


@end
