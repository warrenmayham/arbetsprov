//
//  FirstViewController.h
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCImages: UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@end

