//
//  SecondViewController.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "VCPhonebook.h"
#import "MyManager.h"

@interface VCPhonebook ()

@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

/*
 Phonebook sköter interfacet för vy 2, men det mesta för vy 2 har hamnat i VCContactlist
 */

@implementation VCPhonebook

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.textField.keyboardType = UIKeyboardTypeNumberPad;

}

- (void)viewWillAppear:(BOOL)animated{
    MyManager *sharedManager = [MyManager sharedManager];
    
    if(sharedManager.currPhoneNumber != nil){
        self.textField.text = sharedManager.currPhoneNumber;
    }else{
        self.textField.text = nil;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.textField resignFirstResponder];
    MyManager *sharedManager = [MyManager sharedManager];
    
    if(self.textField.text != nil){
        [sharedManager setPhoneNumber:self.textField.text]; //sparar textfältet när man trycker ner keyboard.
    }else{
        [sharedManager setPhoneNumber:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openPhonebook:(id)sender {
    
    [self performSegueWithIdentifier:@"goToContactList" sender:self];
}



@end
