//
//  FirstViewController.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-08-08.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "VCImages.h"
#import "CustomCell.h"
@interface VCImages ()

@property (weak, nonatomic) IBOutlet UICollectionView *collView;

@end

@implementation VCImages{
    NSArray *urlArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    urlArray = [NSArray arrayWithObjects:@"https://upload.wikimedia.org/wikipedia/commons/d/da/Internet2.jpg", @"https://bithin.files.wordpress.com/2012/02/https.jpg", @"https://www.eff.org/files/https-everywhere-button.png", @"https://topdog.nu/wp-content/uploads/2015/06/HTTPS.jpg", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return urlArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellID" forIndexPath:indexPath];
    cell.imageView.image = nil;
    [cell.spinner setHidden:false];
    [cell.errorMessage setHidden:true];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       NSURL *imageURL = [NSURL URLWithString:[urlArray objectAtIndex:indexPath.row]];
                       NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                       
                       dispatch_sync(dispatch_get_main_queue(), ^{
                           
                           cell.imageView.image = [UIImage imageWithData:imageData];
                           [cell.spinner setHidden:true]; //Tar bort spinnern
                           if(cell.imageView.image == nil){
                               [cell.errorMessage setHidden:false]; //Visar simpelt felmeddelande om ingen bild finns
                           }
                           
                       });
                   });
    
    return cell;
}
@end
