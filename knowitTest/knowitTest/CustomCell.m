//
//  CustomCell.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-09-13.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "CustomCell.h"

/* This class is a custom made cell for collectionview. It's used to represent
 cells and transfer properties through its connected IBOutlets   */

@implementation CustomCell

- (id) init{
    if ([super init]){
        
    }
    return self;
}

@end
