//
//  VCVoice.m
//  knowitTest
//
//  Created by Martin Wilhelmsson on 2016-09-07.
//  Copyright © 2016 Martin Wilhelmsson. All rights reserved.
//

#import "VCVoice.h"
#import <AVFoundation/AVFoundation.h>

@interface VCVoice ()

@property (weak, nonatomic) IBOutlet UITextField *text;
@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;
@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet UIButton *language;


@end

@implementation VCVoice{
    int totalUtterances;  //number of queued utterances
    int currentUtterance; //number of the currently spoken utterance
    int totalTextLength;  //length of the whole text
    int spokenTextLenghts;//stores the lengths of the texts that have already been spoken
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated{
    self.synthesizer = [[AVSpeechSynthesizer alloc]init];
    self.synthesizer.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.text resignFirstResponder];
}

- (IBAction)playPauseButton:(id)sender {
    
    if(self.synthesizer.isPaused){ //synth paused
        //NSLog(@"Paused");
        [self.synthesizer continueSpeaking];
        [self.play setTitle:@"Pause" forState:UIControlStateNormal];
        
    }else if(self.synthesizer.isSpeaking == YES){ //synth speaking
        //NSLog(@"Speaking");
        [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        [self.play setTitle:@"Resume" forState:UIControlStateNormal];
        
    }else{ //synth not active
        //NSLog(@"Inactive");
        NSString *voiceText;
        
        if(self.text.text == nil || [self.text.text isEqual: @""]){
            voiceText = self.text.placeholder;
        }else{
            voiceText = self.text.text;
        }
        
        [self.play setTitle:@"Pause" forState:UIControlStateNormal];
        AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString: voiceText];
        //[utterance setRate:0.8f];
        
        if([self.language.titleLabel.text isEqual: @"English"]){
            utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-US"];
        }else{
            utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"sv-SE"];
        }
        
        [self.synthesizer speakUtterance:utterance];
    }

}

- (IBAction)stopButton:(id)sender {
    [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

- (IBAction)languageButton:(id)sender {
    if([self.language.titleLabel.text isEqual: @"English"]){
        [self.language setTitle:@"Svenska" forState:UIControlStateNormal];
        self.text.placeholder = @"Det här är din röst.";
        
    }else{
        [self.language setTitle:@"English" forState:UIControlStateNormal];
        self.text.placeholder = @"This is your voice.";
    }
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    [self.play setTitle:@"Play" forState:UIControlStateNormal];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance{
    
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer willSpeakRangeOfSpeechString:(NSRange)characterRange utterance:(AVSpeechUtterance *)utterance{
    
}

@end
