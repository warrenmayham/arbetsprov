# README #

2016-08-08 Martin Wilhelmsson

**This project exists to build small views with minimalistic functionality to display parts of my programming ability.**

## View 1 - Image Download ##

Downloads and display 4 images from the Internet. The images are downloaded in parallel and each image is displayed as soon as it has been downloaded. The images are downloaded from different servers on the Internet and it is indicated if the image is being loaded or if an error has occurred.

## View 2 - Pick a contact from address book ##

The Phone Book is accessed and the user can select a phone number. The selected phone number is presented in a text input box.

## View 3 - Dial a number ##

It is possible to enter a phone number. The phone number is dialed when the user
presses a button. The phone number from View 2 is pre-entered. A confirmation alert is shown to the user when the call is about to be made, giving the user a chance to abort.

## View 4 - Voice synthesizer ##

The user can write or paste a text into a text field, which can subsequently be read out by the voice synth in English or Swedish. The playing can be paused or aborted.